
#import ankiVector
from anki_vector.connection import ControlPriorityLevel
import anki_vector
from anki_vector.util import distance_mm, speed_mmps, degrees
from anki_vector.behavior import MIN_HEAD_ANGLE, MAX_HEAD_ANGLE
from anki_vector import events
#import opencv
import cv2
#import Pil pour le traitement des images
import PIL
from PIL import Image  
#import numpy pour le transformer une image en tuple
import numpy as np
#import dlib pour tranformer un visage en vector
import dlib
#import autre pour la reconnaissance facial 
from imutils import face_utils
import argparse
from pathlib import Path
import os 
import time
import ntpath

#fonction pour transformer les visage connu en tuples
def loadface():
    print('[INFO] Importing pretrained model..')
    pose_predictor_68_point = dlib.shape_predictor("pretrained_model/shape_predictor_68_face_landmarks.dat")
    pose_predictor_5_point = dlib.shape_predictor("pretrained_model/shape_predictor_5_face_landmarks.dat")
    face_encoder = dlib.face_recognition_model_v1("pretrained_model/dlib_face_recognition_resnet_model_v1.dat")
    face_detector = dlib.get_frontal_face_detector()
    print('[INFO] Importing pretrained model..')
    return [pose_predictor_68_point, pose_predictor_5_point, face_encoder, face_detector]


#Lancement du robot
print('[INFO] Starting System...')
#chargement de visage
startLoadFace = loadface()
pose_predictor_68_point = startLoadFace[0]
pose_predictor_5_point = startLoadFace[1]
face_encoder = startLoadFace[2]
face_detector = startLoadFace[3]

def newframe(robot):
    robot.camera.init_camera_feed()
    images = robot.camera.latest_image.raw_image
    images.save('RobotSave.png')
    frame = np.asarray(PIL.Image.open('RobotSave.png'))
    return [frame,images]

#lancement du robot
args = anki_vector.util.parse_command_args()
with anki_vector.Robot() as robot:
    def transform(image, face_locations):
        coord_faces = []
        for face in face_locations:
            rect = face.top(), face.right(), face.bottom(), face.left()
            coord_face = max(rect[0], 0), min(rect[1], image.shape[1]), min(rect[2], image.shape[0]), max(rect[3], 0)
            coord_faces.append(coord_face)
        return coord_faces


    def encode_face(image):
        face_locations = face_detector(image, 1)
        face_encodings_list = []
        landmarks_list = []
        for face_location in face_locations:
            # DETECT FACES
            shape = pose_predictor_68_point(image, face_location)
            face_encodings_list.append(np.array(face_encoder.compute_face_descriptor(image, shape, num_jitters=1)))
            # GET LANDMARKS
            shape = face_utils.shape_to_np(shape)
            landmarks_list.append(shape)
        face_locations = transform(image, face_locations)
        return face_encodings_list, face_locations, landmarks_list


    def easy_face_reco(frame, known_face_encodings, known_face_names):
        rgb_small_frame = frame[:, :, ::-1]
        # ENCODING FACE
        face_encodings_list, face_locations_list, landmarks_list = encode_face(rgb_small_frame)
        face_names = []
        name = ""
        num_face = 1
        for face_encoding in face_encodings_list:
            if len(face_encoding) == 0:
                return np.empty((0))
            # CHECK DISTANCE BETWEEN KNOWN FACES AND FACES DETECTED
            vectors = np.linalg.norm(known_face_encodings - face_encoding, axis=1)
            tolerance = 0.6
            result = []
            for vector in vectors:
                if vector <= tolerance:
                    result.append(True)
                else:
                    result.append(False)
            if True in result:
                first_match_index = result.index(True)
                name = known_face_names[first_match_index]
            else:
                name = str(num_face) + "-Qui est ce ?"
            num_face += 1
            face_names.append(name)

        left_face = 0
        top_face = 0
        width_face = 0
        height_face = 0
        bottom_face = 0
        right_face = 0
        for (top, right, bottom, left), name in zip(face_locations_list, face_names):
            cv2.rectangle(frame, (left - 50 , top - 50), (right+50, bottom +50) , (0, 255, 255), 2)#tour
            cv2.rectangle(frame, (left - 50, bottom +20), (right +50, bottom +50), (0, 255, 255), cv2.FILLED)#Prenim
            cv2.putText(frame, name, (left - 40, bottom + 45), cv2.FONT_HERSHEY_TRIPLEX, 0.8, (255, 0, 0), 1)
            # print(str(top) + "t " + str(right) + "r " + str(bottom) + "b " + str(left) + "l ")
            width_face  = 640 - (left + right)
            height_face  = 360 - (top + bottom)
            left_face = left - 10
            top_face = top -20
            bottom_face = bottom + 20
            right_face = right + 10
        for shape in landmarks_list:
            for (x, y) in shape:
                cv2.circle(frame, (x, y), 1, (255, 0, 255), -1)  
        return [name, left_face, top_face, right_face, bottom_face, num_face]
    
    #Sauvegarde une nouvelle photo avec le nom
    def saveface(left_face, top_face, right_face, bottom_face):
        print(str(left_face) + 'lf ' + str(top_face) + 'tp ' + str(bottom_face) + 'bt ' + str(right_face) + 'rg ')
        checkframe = input("Voulez vous vous identifier (y/n):")
        if checkframe == 'y':
            face_name = Image.open('RobotSave.png')
            box = (left_face, top_face, right_face, bottom_face)
            face_area = face_name.crop(box)
            face_area.show()
            username = input("Veuillez identifier cette personne sur la photo:")
            face_area.save('known_faces/' + username + '.png')
            face_user = 'Face identified : '+ username
            face_area.close()
            chargerfaces()
        elif checkframe == 'n':
            print('Personne non identifié')
            face_user = 'Face unidentified'
        else:
            print('Mauvaise reponse veuillz saisir y ou n')
            face_user = 'No faces'
            saveface(left_face, top_face, right_face, bottom_face )
        return face_user

    def chargerfaces():
        print('[INFO] Importing faces...')
        face_to_encode_path = Path("known_faces")
        files = [file_ for file_ in face_to_encode_path.rglob('*.jpg')]

        for file_ in face_to_encode_path.rglob('*.png'):
            files.append(file_)
        if len(files)==0:
            raise ValueError('No faces detect in the directory: {}'.format(face_to_encode_path))
        known_face_names = [os.path.splitext(ntpath.basename(file_))[0] for file_ in files]

        known_face_encodings = []
        for file_ in files:
            image = PIL.Image.open(file_)
            image = np.array(image)
            face_encoded = encode_face(image)[0][0]
            known_face_encodings.append(face_encoded)

        print('[INFO] Faces well imported')
        print('[INFO] Starting Webcam...')
        print('[INFO] Webcam well started')
        print('[INFO] Detecting...')
        run0 = ''
        face_user = ''
        name_face = ''
        while True:
            frame = newframe(robot)
            name = easy_face_reco(frame[0], known_face_encodings, known_face_names)
            if name[0].find('-') > 0:
                name_tab = name[0].split('-')
                name_face = name_tab[1]
            else:
                name_face = name[0]
            if run0 == "Qui est ce ?":
                cv2.imshow('Easy Facial Recognition App', frame[0])
                print(str(name[1]) + 'l ' + str(name[2]) + 't ' + str(name[3]) + 'b ' + str(name[4]) + 'r ')
                face_user = saveface(name[1],name[2],name[3],name[4])
                
            else:
                cv2.imshow('Easy Facial Recognition App', frame[0])
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            run0 = name_face


    if __name__ == '__main__':
        chargerfaces()
        
        print('[INFO] Stopping System')
        video_capture.release()
        cv2.destroyAllWindows()
